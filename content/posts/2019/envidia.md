---
title: "Envidia"
date: "2019-12-27"
description: "La RAE la ha definido como tristeza o pesar del bien ajeno, o como deseo de algo que no se posee."
type: "publicaciones"
tags:
  [
    "bien",
    "mal",
    "envidia",
    emociones,
    sentimientos,
    vida,
    sentir,
    vivir,
    emociones,
    sociedad,
    sentimientos,
  ]
category: ["Publicaciones"]
img: "https://img.bekiapadres.com/articulos/portada/38000/38969.jpg"
---

La envidia es una moneda corriente en las sociedades desde los orígenes de las mismas. Cuántas veces nos dimos cuenta de que la gente tiene ese sentimiento hacia nosotros, o porque no, cuántas veces la sentimos en carne propia por diversos motivos.

Lo más importante para poder comenzar a entender esto es sacarnos la etiqueta de qué “es un sentimiento malo” (dado que no existen sentimientos malos ni buenos, son solo sentimientos y ya) y que muchas veces son una forma de impulso o de conexión con esa persona o cosa que desata dicho sentimiento.

![](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F1.bp.blogspot.com%2F-T8jMx4IUBdk%2FU6HsgjCvDOI%2FAAAAAAAABfI%2FZt6t6uwnBrU%2Fs1600%2FEnvidia%2B1.jpg&f=1&nofb=1)

Por ejemplo, una persona que estudia en conjunto con nosotros o requiere de nuestra ayuda para comprender un tema termina puntuado mejor que nosotros en un examen. Muchos podemos llegar a sentir envidia de esa nota (El sentimiento de que merecíamos esa nota mucho más que la otra persona) y hasta ahí vamos todo bien. Sin embargo, dependiendo de cómo usemos y sintamos este evento es como puede convertirse en un sentimiento positivo o negativo para nuestro crecimiento.

Si nos dedicamos a excusar esa diferencia de puntuación, no vamos a poder enriquecernos y perderíamos una oportunidad muy rica en conocimiento. En cambio, si nosotros transformamos ese sentimiento en orgullo a nuestro compañero que logró entenderlo o comprenderlo y además implementarlo correctamente. También tenemos cuota de orgullo a nosotros por formar parte de su aprendizaje, y poder llegar a transferir conocimiento.

Cuando uno decide envidiar sanamente es capaz de encontrar felicidad en eso que buscamos y otra persona encuentra, imaginen lo hermoso que sería poder sentirnos felices de los progresos ajenos como si fuesen nuestros.
