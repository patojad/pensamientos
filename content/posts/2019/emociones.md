---
title: "Emociones"
date: "2019-12-17"
description: "¿Le das espacio a tus sentimientos? ¿Vives cada uno de ellos? ¿Crees que lo sentimientos o emociones son tan importantes?¿Que lugar les das en tu vida?"
type: "publicaciones"
tags:
  [
    "bien",
    "mal",
    "sociedad",
    emociones,
    sentimientos,
    vida,
    sentir,
    vivir,
    emociones,
    sociedad,
    sentimientos,
  ]
category: ["Publicaciones"]
img: "https://www.educacionadventista.com/wp-content/uploads/2023/06/Educar-las-emociones-miedo-verguenza-ENTRADA-e1695221843217.jpg"
---

Según la idea que se persigue en las sociedades los sentimientos o las emociones tienden a encasillarse en bueno o malo. Y de ahí es como nosotros tendemos a reprimir algunas emociones y a demostrar otras emociones que en algunas ocasiones no so propias o no llegamos a sentir. Poniendo asi una mascara a nuestra realidad.

Nosotros tomamos como mal sentimiento, por ejemplo la tristeza, por lo cual, no nos tomamos el tiempo de sentir esa tristeza o vivirla como lo que realmente es (un sentiemiento más), y lo que hacemos es demostrar una falsa alegría que sólo enmascara lo que está pasado. ¿y esto porque lo hacemos? porque la sociedad dice que es mejor sonreír y dando una connotacion negativa a los otros estados de animo. Esto nos lleva a no vivir o sentir correctamente nuestro sentimiento hasta el punto de desvanecerse y no vivir ni sentir la alegría que estamos fingiendo.

![](https://www.quo.es/wp-content/uploads/2019/10/te-imaginas-no-tener-sentimientos-1280x640.jpg)

En este punto estamos viendo que muchas cosas se las atribuimos a la sociedad (No solamente en esta publicación) y no necesariamente porque esta es mala. Simplemente "la sociedad" (Hablaremos un poco mas de este termino mas adelante) nos inculca cosas que no nos permite pensar o dudar porque es algo que viene así, y no importa cuantos estén en contra porque "la sociedad es así". De cierta forma nos quita individualismo, generando un comportamiento en cadena que pocos comparten e inculso menos entienden.

¿Le das espacio a tus sentimientos? ¿Vives cada uno de ellos? ¿Crees que lo sentimientos o emociones son tan importantes?¿Que lugar les das en tu vida?
