---
title: "Distancia"
date: "2019-12-12"
description: "Tomar distancia es una de las pocas cosas que se pueden hacer en cualquier momento para estar bien..."
type: "publicaciones"
tags:
  [
    "distancia",
    "pensamientos",
    "sociedad",
    "salud mental",
    "soledad",
    "miedo",
    "comodidad",
    "independencia",
    "afecto",
    "espacio",
    "seres vivos",
    "30 minutos",
    "entendernos",
    "nosotros",
  ]
category: ["Publicaciones"]
img: "https://apmg-international.com/sites/default/files/images/illustrations/header/illu_us_office.jpeg"
---

Muchas veces debemos tomar distancia de los demás, y no necesariamente física. Tenemos que saber alejarnos de las cosas o personas que nos rodean y de su sentir. Está puede ser una forma de buscar claridad.

El ser humano tiene tanto miedo, constantemente, que le impide alejarse de esa zona de confort. Tanto miedo que no es capaz de vivir su vida completamente.

![](https://okdiario.com/img/2018/12/13/disfrutar-la-soledad-es-sinonimo-de-salud-mental-segun-la-ciencia-655x368.jpg)

Alejarnos no es solamente alejarse de las personas o de las cosas. También alejarnos de los miedos, los sentimientos, y llegando a una distancia prudencial de esas situaciones podemos llegar a entendernos mejor con nosotros.

Qué tan difícil se vuelve para nosotros valorar un segundo con nosotros mismos. Apreciaremos como seres vivos, ver nuestra independencia completa. Estamos acostumbrados a necesitar afecto y compartir nuestro espacio que cuando no lo hacemos nos sentimos incompletos.

Sería hermoso poder dedicarnos todos los días unos 30 minutos para estar con nosotros mismos. Tomar distancia de nuestro entorno y conocernos mejor
