---
title: "¿Qué está bien?"
date: "2019-11-12"
description: "¿Qué está bien? ¿Qué está mal? Estas preguntas solemos responderlas repitiendo cosas que no sentimos..."
type: "publicaciones"
tags:
  [
    "bien",
    "mal",
    "sociedad",
    "tiempo",
    "presente",
    "futuro",
    "pasado",
    "ahora",
    "momento",
    "vida",
    "sentir",
    "querer",
    "deseo",
    "decisión",
    "rumbo",
    "cambio",
    "sentir",
    "vivo",
    "importante",
    "mente",
    "pensamiento",
    "sentimiento",
    "vivir",
    "entender",
    "realidad",
    "futuro",
    "presente",
    "ahora",
    "tiempo",
    "vida",
    "momento",
    "existir",
    "sentir",
    "hacer",
    "querer",
    "necesitar",
    "importante",
    "entender",
    "vivir",
    "presente",
  ]
category: ["Publicaciones"]
img: "https://excelencemanagement.files.wordpress.com/2017/03/sociedad-rendimiento.jpg?w=606&h=328"
---

Desde que nacemos cargamos con el peso de una sociedad que nos dice según algunas personas o algunos conceptos tal vez muy desactualizados cómo se supones que debemos vivir, qué es lo que hay que hacer, que es lo que está bien...

Es común que al pasar el tiempo dudemos de muchas cosas. Salvó la propia naturaleza todo lo demás está mal. Y vean que irónicamente salvó la naturaleza todo lo demás lo hizo el humano.

## ¿Realmente todo está mal?

Si y no, vivir con una farsa impuesta está mal, pensar no; destruir la naturaleza está mal, hacer pequeñas edificaciones con ella no; al menos, para mí, es así lo cual no asegura ninguna verdad simplemente un punto de vista. Aunque deberíamos centrarnos en lo mas importante... tus propios pensamientos ¿Y para vos? ¿Qué está bien? ¿Qué está mal? ¿Haces eso que para vos está mal solo porque la sociedad dice que está bien?

El problema fue ese momento en el que pasamos de buscar una forma más cómoda de vivir a ponerle un precio y un estandarte. Ese momento en el que empezamos a medir a todas las personas como si fueran robot de industrias. En momento que dejamos que la creencia de pocos se volviera más importante que nuestra naturaleza o nuestro propio pensar (que no se confunda, no hablo de un sistema anárquico). En ese momento perdimos el control de nuestra vida como tal, o mejor lo dicho, lo cedimos a unos estandartes socio-culturales con los cuales no siempre estamos de acuerdo, pero seguimos tomándolos como verdades absolutas.

## Situación

¿Cuántas veces nos detenemos a pensar si estamos de acuerdo realmente con lo que estamos haciendo? En el transporte público me detengo lentamente a apreciar a la gente que me rodea, es lunes y son 7:50 AM imaginen... ¿cuántas personas están sonriendo? A lo que alcanza mi vista nadie, en el barullo escucho gente preocupada por el dinero, gente sin ganas de ir a trabajar. Puedo ver gente con cara cansada y con la cabeza llena de pensamientos mientras una leve preocupación parece dibujarse en sus rostros inmersos en la digitalización de una aplicación de su celular. Si de algo estoy seguro es que nadie está apreciando este momento como tal, nadie se dió cuenta de lo más importante: Nadie reaccionó en que está vivo y que si hay algo que realmente le pertenece es su vida y puede hacer lo que quiera con ella...
