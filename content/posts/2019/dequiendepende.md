---
title: "¿De quien depende?"
date: "2019-11-25"
description: "Pasamos gran parte de nuestra vida excusando y distribuyendo culpa en las demás personas, ¿Cuando tomaremos la responsabilidad?"
type: "publicaciones"
tags:
  [
    "responsabilidad",
    "nosotros",
    "egoismo",
    "culpa",
    "excusas",
    "reaccion",
    "situacion",
    "cambio",
    "problema",
    "sergi torres",
    "conciencia",
    "elecciones",
    "entorno",
    "malestar",
    "situacion",
    "depende",
    "dependencias",
    "dependencia",
  ]
category: ["Publicaciones"]
img: "https://1.bp.blogspot.com/-y1URy6buiXQ/WuNfp_vY_-I/AAAAAAAAZJE/8L0mm-2ffeE3s0njXG5dwvRpqwdhA_vRACLcBGAs/s640/como-tener-confianza-en-uno-mismo-2.jpg"
---

Pasamos gran parte de nuestra vida excusando y distribuyendo culpa en las demás personas. Si bien los factores externos existen no debemos olvidarnos que nuestras elecciones dependen de nosotros. Como comenta Sergi en sus videos, es importante tomar conciencia de eso.

Hoy en día es común asumir que la culpa del malestar de uno lo tiene una persona o un entorno ¿Pero que tan real es esto? Veámoslo así. Si tu entorno es capaz de generarte malestar, esto quiere decir que si en tu lugar ponemos otra persona debería sentir lo mismo. Sin embargo esto no ocurre. Otra persona puede estar pasando la misma experiencia que nosotros y lo puede estar viviendo completamente diferente.

![](https://concepto.de/wp-content/uploads/2013/05/responsabilidad-e1551992833255.jpg)

Con esto tenemos que pararnos a pensar un poco. Si en esta situación otra persona puede sonreír o simplemente se mantiene inmutable ¿No significa entonces que soy yo el que se enoja(frustra, entristece, etc.) por la situación? Entonces el problema no es la situación. ¿Puede que el problema sea mi reacción ante la situación? Y si el problema es mi reacción, significa que el problema es mio. Y si el problema es mio yo lo puedo cambiar. Y si yo lo puedo cambiar no existe problema, porque lo puedo cambiar.
