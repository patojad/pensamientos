---
title: "El presente"
date: "2019-12-03"
description: "¿Qué está bien? ¿Qué está mal? Estas preguntas solemos responderlas repitiendo cosas que no sentimos..."
type: "publicaciones"
tags:
  [
    "bien",
    "mal",
    "sociedad",
    "tiempo",
    "presente",
    "futuro",
    "pasado",
    "ahora",
    "momento",
    "vida",
    "sentir",
    "querer",
    "deseo",
    "decisión",
    "rumbo",
    "cambio",
    "sentir",
    "vivo",
    "importante",
    "mente",
    "pensamiento",
    "sentimiento",
    "vivir",
    "entender",
    "realidad",
    "futuro",
    "presente",
    "ahora",
    "tiempo",
    "vida",
    "momento",
    "existir",
    "sentir",
    "hacer",
    "querer",
    "necesitar",
    "importante",
    "entender",
    "vivir",
    "presente",
  ]
category: ["Publicaciones"]
img: "https://i.postimg.cc/Jn6KttpW/te-obsesiona-perder-el-tiempo-sl-HBJRsnn-1256x620-1.png"
---

Pasamos nuestra vida mirando que vamos a hacer del futuro, cómo llegar a eso que queremos. Lamentándonos del pasado, por qué lo hicimos así, o porque se dio así. Sin embargo, nunca le prestamos atención al único momento en el que nuestra conciencia entra en coherencia con nuestro entorno. ¡El Presente!

Si nos ponemos a pensar no sabemos con certeza qué es lo que va a pasar a futuro incluso no sabemos si los pensamientos van a ser los mismos. También es algo lógico que no podamos modificar el pasado, ni pensar en un multiverso diferente de qué pasaría si no hubiéramos tomado esa decisión dado que es prácticamente imposible saberlo. Lo único que podemos apreciar es el presente, el ahora, este momento en el cual la vida se hace presente y es en si misma.

Tenemos un único momento en el que existimos y es ahora, lo único que importa es que sentimos ahora, que hacemos ahora y que queremos ahora. No sirve de nada pensar en que voy a querer mañana, porque no puedo saciar eso ahora, ahora solo puedo saciar lo que quiero o necesito ahora. (Con esto no quiero decir que es inútil plantear un futuro). La interpretación de lo que uno desea para un futuro es absurda desde el punto de vida que el futuro nunca llega, porque es eso mismo, el futuro, no es un lugar donde podamos estar o vivir, solo podemos estar o vivir en el único momento posible, el presente.

![](https://i.postimg.cc/C59NHXyP/que-vale-mas-el-tiempo-o-el-dinero.jpg)

Veamos un poco el tema del futuro. Nosotros podemos tener un camino que queremos seguir, pero sepamos que eso es lo que hoy queremos. No lo que vamos a querer en el futuro, hoy quiero eso. Y también hay que entender que el camino para obtener eso inicia ahora, en este momento. Es posible que en “en el futuro” cambiemos el rumbo. Pero no es algo que podamos ver, predecir o entender certeramente en el hoy, porque solo podemos vivir en el presente y entender este momento.

Te propongo ahora darte un tiempo, sentir tu presente y darte cuenta de que estas vivo, justo ahora en este momento y que eso es lo más importante y lo que estás sintiendo ahora es lo importante. Entonces tomate el tiempo de sentir eso. Es importante, también, entender dónde estamos viviendo, que tan presente estamos, que tan importante es para nosotros lo que estamos viviendo y en dónde esta puesta nuestra mente.
