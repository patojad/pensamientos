---
title: "Nosotros Mismos"
date: "2019-11-19"
description: "Si bien puede sonar egoísta una parte importante a lo largo de nuestra vida es pasar tiempo con nosotros mismos, escuchar lo que pensamos y sentimos"
type: "publicaciones"
tags:
  [
    "individualidad",
    "nosotros",
    "egoismo",
    "sociedad",
    "sentir",
    "pensar",
    "sentir",
    "respeto",
    "integridad",
    "conocimiento",
    "miedo",
    "debilidad",
    "puntos debiles",
    "toxicidad",
    "grupos",
    "jovenes",
    "conductas",
    "ideales",
    "miedo",
    "rechazo",
    "pertener",
    "precio",
    "agresiones",
    "debilidad",
    "miedos",
    "sociales",
    "respeto",
    "persona",
    "integridad",
    "expresarte",
    "pensar",
    "libremente",
    "dudar",
    "titubear",
    "equivocarte",
    "respeto",
    "persona",
    "integridad",
  ]
category: ["Publicaciones"]
img: "https://psicocode.com/wp-content/uploads/2016/06/ego-1.jpg"
---

Si bien puede sonar egoísta una parte importante a lo largo de nuestra vida es pasar tiempo con nosotros mismos, escuchar lo que pensamos y sentimos nosotros. Eventualmente nos encontramos bloqueados por ideologías o pensamientos externos que no nos dejan terminar de conocernos. Y cómo podemos pretender entender o conocer a otras personas si no lo hacemos con nosotros mismos.

Es común ver esa "toxicidad" que se autogenera la gente con tal de sentirse miembro de "la sociedad" (Quien irónicamente son grupos de personas que no terminan de conocerse a sí misma, en su mayoría, pero creen poder conocer a los demás). Es increíble ver grupos de jóvenes que van formando conductas o ideales en base a lo que solo un miembro cree, hace o dice. Esto se da naturalmente por el miedo a ser rechazado ante un grupo y no conseguir el sentido de pertenencia que tanto buscamos, aunque el precio por esto es dejar de mostrarnos como realmente somos. De sufrir esas agresiones que dejan expuesta nuestra debilidad o nuestros puntos débiles.

![](https://cdn.tn.com.ar/sites/default/files/styles/1366x765/public/2019/08/22/diseno_sin_titulo_79.jpg)

Esto último tiene una parte irreal, lo que realmente quedan expuestos son nuestros miedos. Miedos sociales, muchas veces siquiera son nuestros, pero nos hacemos con ellos solo para sentirnos parte de esta sociedad.

Hoy en particular no existe más remedio para esto que aprender a respetarse uno mismo y entender que la gente que está a tu lado tiene que apreciarte por lo que realmente eres y no porque intentas demostrar que eres en un estandarte social. Siente libre de expresarte y pensar libremente, puedes dudar, titubear y equivocarte, pero nunca pierdas el respeto a tu persona y tu integridad.
