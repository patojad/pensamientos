---
title: "Volver a pensar"
date: "2024-04-26 10:18:00"
description: "Estos dias me encontre en la duda de ¿por que decidi escribir?, ¿por que abri tantos blogs, con diferentes temas? Si de joven la escritura y yo no parecíamos buenos amigos."
type: "publicaciones"
tags:
  [
    pensar,
    escribir,
    blog,
    reflexion,
    pensamientos,
    dudas,
    existencia,
    egoismo,
    crecimiento,
    critico,
    injusto,
    incongruencias,
    absolutismos,
    divagar,
    podcast,
    subtitulos,
    regular,
    compromiso,
    identificado,
    atacado,
    discrepar,
    pensamientos,
    absolutismos,
  ]
category: ["Publicaciones"]
img: https://assets-global.website-files.com/5eb28ea668fa3d1be901d9d1/5eb28ea668fa3d375201e460_desansiedad-como-dejar-de-pensar-tanto-en-la-ansiedad.jpg
---

Pasaron realmente años desde la última vez que redacte algo pensando en volver a publicar, a caso ¿tiene sentido publicar un pensamiento? ¿A quien podría interesarle?

## Porque lo hago

Retornar esto es igual de egoísta que mi blog de tecnología, siempre la motivación principal es crecer… Dentro de la tecnología me obligó a estar aprendiendo cosas nuevas, mantener un ritmo de actualización constante. Pero acá… Bueno acá es diferente, se trata de poner en palabras lo que pienso… Lo que entiendo… 

Al ver el blog en si me recuerda como construir algunos de mis pensamientos o dudas existenciales, me arrepiento de no haber registrado todo el proceso, podría ser interesante entender como algunos absolutismos que creí antes hoy los tacho de absurdos.

## Muchas ideas…

No sé que tan regular pueda ser con esto, no se que busco, no sé que sentido tiene… ¿Podría hablarlo en una especie de podcast? Me resulta una propuesta interesante, pero no tiene sentido, no lo encuentro. Me interesa mucho más entender de que estoy echó, que pasa en mi cabeza y como eso va cobrando algún sentido.

## No hay promesas, solo intenciones

No prometo volver a redactar siempre, aunque mi intención es compartir lo que pienso es muy difícil despersonificar muchas cosas… Y siento que con el tiempo me volví más crítico de las incongruencias ajenas, algo que, talvez, deba replantearme, pero es difícil no pensar en lo injusto que pueden ser las personas solo juzgando al resto…

## Sin irme del tema

Algo completamente difícil para mí es no divagar en lo que pienso, por eso suelo cortar en pequeños subtitulos que me obliguen a volver, siento que no fui, solo perdí registro, pero aún así planeo volver a escribir, no se por cuánto pero va a ser divertido verlo juntos, si estás leyendo esto seguramente sos un familiar que lo hace por compromiso, algún amigo/colega pensando que tengo tiempo al pedo o que hablo boludeces o algún ajeno a mi entorno que piensa “que se fumo”

Descomprometete de la vida, si pensás leer artículos anteriores o aquellos que valla a escribir más adelante solo pensa en que hay pensamientos, podés sentirte identificado, atacado, discrepar, lo que sea… Pero son pensamientos y no absolutismos…