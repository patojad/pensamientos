---
title: "Aplica para otros no para mi"
date: "2024-08-02 10:26:00"
description: "Estos dias me encontre en la duda de ¿por que decidi escribir?, ¿por que abri tantos blogs, con diferentes temas? Si de joven la escritura y yo no parecíamos buenos amigos."
type: "publicaciones"
tags:
  [
    pensar,
    escribir,
    reflexion,
    pensamientos,
    dudas,
    reglas,
    pensamientos,
    absolutismos,
  ]
category: ["Publicaciones"]
img: https://s2.abcstatics.com/abc/www/multimedia/bienestar/2024/02/26/hipocresia-RS51lc9ZmCTdvMUTX2jmXQK-1200x840@diario_abc.jpeg
---

Es increíble está sensación que maneje toda la vida, puedo verlo en todas las personas, incluso en mí, si bien siento que personalmente tengo que mejorar en muchas cosas. Está es particularmente la que más daño me hace, y si bien es bastante común en las personas me desagrada tanto que cuando veo que hice lo mismo me doy asco, y si esa es la palabra, asco…

¿No les pasó de estar con una persona que critica a otra persona por algo que ella también hace? Esto es, en base, el gran problema, no porque veamos cosas malas en los demás que de pronto nos puede pasar a nosotros, sino que el echo de creer que nuestra justificación es más válida que la de la otra persona es lo que está mal. Vi, por ejemplo, parejas que se recriminan cosas que ellos mismos se hacen, desde algo tran trivial como cosquillas a algo más socialmente inaceptable como la infidelidad. Y siempre el “dominante” acusa al “victimario” por algo que esa persona también hace, apelando a qué su justificación es más válida que la de la otra persona

Y a todo esto, yo me pregunto… ¿Dónde quedó el “no hacer lo que no nos gusta que nos hagan”? Si bien en cosas más triviales como las cosquillas pueden tener límites más “negociables” de seguro en temas más complejos es difícil encontrar el punto intermedio 

## ¿Existe punto intermedio?

Si bien la respuesta clara parece ser que si, en la implementación es un poco más complicado. No me refiero a que no se puedan encontrar puntos intermedios en estos casos (ya sean triviales o no) si o de la duración de esos puntos intermedios

Si vemos la relación como una balanza, estos puntos intermedios como “Vos me podés hacer cosquillas a mi porque no me molesta pero yo no a vos” desequilibran está balanza, y más cuando las cosquillas se vuelven moneda corriente en la relación

> Cuando se habla de relación no necesariamente hablamos de una pareja carnal, también es una relación afectiva dónde no haya juego de poderes

Por esto mismo es importante revisar esos puntos medios constantemente o en el caso opuesto usar sistema tradicional de “todos o ninguno” que si bien es más práctico también reduce los matices en las relaciones (algo que desde mi perspectiva es muy importante)

## Volviendo al origen 

Esto, que describí al inicio, no es algo que pase solamente en relaciones, sino que algo socialmente común. Por ejemplo cuando tú vecina critica a la nueva vecina del barrio por salir todas las noches cuando su hija hace lo mismo 

En estos casos no solemos tomar ninguna medida, y eso transforma estás críticas en un veneno que ingresa de a poco en nosotros y nos lleva a volvernos partes de está metodología de “si yo lo hago está bien, si el lo hace está mal”

Si no podemos volvernos personas activas a estos atropellos morales, sin llegar a la confrontación, entonces debemos evitar esos momentos y ser complices o aceptar esos tratos que nos destruyen