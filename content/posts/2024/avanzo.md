---
title: "Avanzo"
date: "2024-07-19 02:18:00"
description: "Estos dias me encontre en la duda de ¿por que decidi escribir?, ¿por que abri tantos blogs, con diferentes temas? Si de joven la escritura y yo no parecíamos buenos amigos."
type: "publicaciones"
tags:
  [
    pensar,
    avanzo,
    avanzar,
    problemas,
    humor,
    liderazgo,
    lider,
    liderar,
    familia,
    miedo,
    miedos,
    angustia,
    tristeza,
    felicidad,
    emociones,
    sentimientos,
    apariencias,
    apariencia,
    comunicacion,
    comunicar,
    guia,
    advertencia,
    dirigente,
    scout,
    marido,
    padre,
    ideal,
    cancion,
    musica,
  ]
category: ["Publicaciones"]
img: https://www.ihelp.org.es/blog/wp-content/uploads/2021/03/ayuda-1060x746.jpg
---

Es increíble ver cómo los momentos nos limitan.¿Cuentas veces tenemos que dejar de sentir y avanzar porque otros lo necesitan? ¿Está bien seguiré avanzando sin fuerzas?

> Si estoy tranquilo, ellos se asustan. Si me asusto, ellos desesperan. Si desespero, ellos mueren...

Está es la carga que llevamos aquellos que lideramos, una vez, una persona que admire mucho me dijo esas palabras, y resonaron en mi cabeza todos los días de mi vida, no puedo darme el lujo de asustarme, mucho menos desesperarme. Y ese es motivo por el cual avanzo.

## Apariencias

Tristemente la gente confunde la calma con el desinterés. El observar con el mirar. El analizar con el no hacer nada. 

Nada me angustia más que menosprecien mis sentimientos solo porque no los comunico “a ti no te importa pero…” cuando ésto pasa, siento que cuidar al resto me hace ver como alguien detestable, ¿Y tiene sentido exponerme de esa forma a cambio de desprecio? Tal vez, sería mejor no cargar con eso, no liderar en esas situaciones. Siempre tiene más crédito quien publica sus miedos a quienes combaten sus demonios internos.

## Más que que por mi, lo hago por ti

Sin embargo, aquellas palabras que suenan a una mochila difícil de llevas venían acompañadas de una guía, o más bien, de una advertencia, y si bien, el diluyó las palabras exactas, su significado está guardado en mí.

> Lo hago por tí, no por mí. No lidero por gusto o poder, lo hago porque lo necesitas, lo hago porque quiero verte bien, y sea cual sea el precio estoy dispuesto a pagarlo

Esto lo aprendí siendo dirigente scout, pero el día que forme una familia estás palabras pesaron más en mi que nunca, si bien se que estoy lejos de ser un marido ideal o un padre ideal, se cómo tengo que actuar, y se que es más por ellos que por mi

## ¿Y por qué avanzo?

No hay secretos o una fórmula mágica para liderar, mucho menos en esos momentos en que nuestro espíritu está completamente destruido, como al momento de escribir esto, simplemente hay que mantenerse en movimiento buscando un sitio en el que está mejor… Pero eso se los puede explicar mejor está canción:

<center>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/IAPrryQL_9o?si=N-dvp4VzGHXV71F1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</center>