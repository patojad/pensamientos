---
title: "¿Por que escribo?"
date: "2020-06-22 10:18:00"
description: "Estos dias me encontre en la duda de ¿por que decidi escribir?, ¿por que abri tantos blogs, con diferentes temas? Si de joven la escritura y yo no parecíamos buenos amigos."
type: "publicaciones"
tags:
  [
    "por",
    "que",
    "escribo",
    "patojad",
    "pensamientos",
    "escritura",
    "blog",
    "tecnologia",
    "comunidad",
    "aprender",
    "conocer",
  ]
category: ["Publicaciones"]
img: https://nuriamasdeu.com/wp-content/uploads/2021/01/Como-empezar-a-escribir.jpg
---

Estos dias me encontre en la duda de ¿por que decidi escribir?, ¿por que abri tantos blogs, con diferentes temas? Si de joven la escritura y yo no parecíamos buenos amigos, yo con mis faltas de ortografía que a ella no le gustaba. Y yo siempre sentí que no tendría nada que darme, mi amor estaba enlazado con los números y la lógica, y hoy me encuentro escribiendo, puede que sonando como un narrador sin voz en la cabeza de alguien que me lee y no me conoce.

Al releer muchos de mis textos puedo entender que para algunos sean cosas profundas, para otros sean estupideces. Puedo ver como una verborragia de palabras que no están del todo bien acomodadas, porque aun me falta aprender mucho, puede ser significativa o parecer basura…

Pero más allá de eso mi pregunta parece ser mucho más simple… ¿Por qué Escribo?¿Por qué esa necesidad de plasmar cosas que muchas veces no le importan a nadie y nadie las pidio?

## Mi camino en la “escritura”

Cuando me pregunte estas cosas fui viendo el camino que hice, ¿Como empezó todo? fue lo que resonaba en mi cabeza en busca de una hilo para tirar y descubrir que había detrás de esta gran incógnita…

Si bien pude ir mas atras, decidi arrancar cuando inicio mi idea de escribir solo, de hacerlo como hobbie, y eso arrancó con PatoJAD…

## PatoJAD la tecnología una parte de mi vida

La tecnología es una parte muy importante en mi vida por que es algo que me gusta y por suerte es lo que me da de comer hoy por hoy. Sin embargo PatoJAD nace como mi deseo de elevar la voz de cualquiera que quiera opinar. En el mundo donde está disperso ese blog solo se encuentran blogs grandes con opiniones extremistas. PatoJAD nace como el lugar donde cualquiera puede poner su opinión y un lugar para respetar a todos… PatoJAD nace como la idea de una comunidad mucho más sana…

¿Para qué escribo en PatoJAD? Para incluir, para que la gente que quiere meterse en ese mundo (de linux) Tenga un lugar donde preguntar y coexistir con otros que piensen diferente en paz. Para aprender, cada publicación me lleva a entender muchas cosas que no sabía y eso me ayuda a crecer…

## Pensamientos

Este blog, cuando nació, quería mostrar un poco más quién era yo… Me di cuenta que eso es absurdo, ¿a quién le importa quien escriba esto? ni siquiera a mi, cada vez que lo leo, en mi cabeza suena una voz de un anónimo… Entendí entonces que eso fue un lindo mensaje para el pie de página. ¿Y entonces?¿Por que me tome el tiempo de armar este blog? Pensé toda lo noche y no me di cuenta que la respuesta era tan simple. Para poder conocerme, para poder leer lo que pienso, para que no quede como algo que se me ocurrió y después murió en el olvido.

¿Para qué escribo en Pensamientos? Porque aun siento que no me termino de conocer y esta es mi forma de saber quien soy. ¿Puede que mis palabras sirvan a otras personas? No tengo forma de saberlo, pero sé que no hago daño escribiendo…

---

Y a vos te pregunto ¿Escribis? ¿Por qué?
